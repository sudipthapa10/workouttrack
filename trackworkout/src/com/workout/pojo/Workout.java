package com.workout.pojo;

import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.google.appengine.api.datastore.Key;

@Entity
public class Workout {
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	List<Set> sets;

	public List<Set> getSets() {
		return sets;
	}

	public void setSets(List<Set> sets) {
		this.sets = sets;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Key id;
	@Basic(optional = false)
	private String name;
	@Basic(optional = false)
	private boolean isSuperset;
	@Basic(optional = false)
	private boolean isDropset;

	public Key getId() {
		return id;
	}

	public void setId(Key id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isSuperset() {
		return isSuperset;
	}

	public void setSuperset(boolean isSuperset) {
		this.isSuperset = isSuperset;
	}

	public boolean isDropset() {
		return isDropset;
	}

	public void setDropset(boolean isDropset) {
		this.isDropset = isDropset;
	}

}
