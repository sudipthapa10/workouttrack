package com.workout.pojo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.google.appengine.api.datastore.Key;

@Entity
public class Rep {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Key id;

	private int weight;
	
	private int repcount;

	public Key getId() {
		return id;
	}

	public void setId(Key id) {
		this.id = id;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public int getRepcount() {
		return repcount;
	}

	public void setRepcount(int repcount) {
		this.repcount = repcount;
	}

	

}
