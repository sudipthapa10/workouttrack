package com.workout;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.workout.pojo.Set;
import com.workout.pojo.Workout;

public class WorkoutController extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
	
		EntityManager mgr1 = getEntityManager();
		Query query = mgr1.createQuery("select from " + Workout.class.getName());
		List<Workout> workout = (List<Workout>) query.getResultList();
		
		Gson gson = new Gson();
		String json = gson.toJson(workout);
		resp.getWriter().write(json);
		
}
	
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		System.out.println("Positing Workout");
		InputStream inputStream = req.getInputStream();
		
		// convert inputStream to string
		String requestString = toString(inputStream);

		Gson gson = new Gson();
		Workout workout1 = gson.fromJson(requestString,Workout.class);
		EntityManager mgr1 = getEntityManager();
	/*	Set set = new Set();
	List<Set> sets = new ArrayList<Set>();
		sets.add(set);
		workout1.setSets(sets);*/
		try {
			// save to database
			mgr1.persist(workout1);
		} finally {
			mgr1.close();
		}
		

}
	private String toString(InputStream inputStream) {
		// TODO Auto-generated method stub
		Scanner scan= new Scanner(inputStream);
		scan.useDelimiter("//cbd");
		return scan.next();
		
	}
	private static  EntityManager getEntityManager() {
		// TODO Auto-generated method stub
		return EMF.get().createEntityManager();
	}
}