package com.workout;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Scanner;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.workout.pojo.User;

@SuppressWarnings("serial")
public class UserController extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		EntityManager mgr = getEntityManager();

		Query query = mgr.createQuery("select from " + User.class.getName());

		List<User> users = (List<User>) query.getResultList();
		
		Gson gson = new Gson();
		String json = gson.toJson(users);
		resp.getWriter().write(json);
	}

	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		System.out.println("Positing user");

		InputStream inputStream = req.getInputStream();

		// convert inputStream to string
		String requestString = toString(inputStream);

		Gson gson = new Gson();
		User user = gson.fromJson(requestString, User.class);

		if (!user.getPassword().equals(user.getPasswordAgain())) {
			resp.getWriter().write("{error: 'Password dont match' }");
			return;
		}

		//update the hash
		try {
			user.setHash(PasswordHash.createHash(user.getPassword()));
		} catch (Exception e) {
			e.printStackTrace();
		}

		EntityManager mgr = getEntityManager();
		try {
			// save to database
			mgr.persist(user);
		} finally {
			mgr.close();
		}
	}

	private String toString(InputStream inputStream) {
		Scanner sc = new Scanner(inputStream);
		sc.useDelimiter("\\Z+");
		return sc.next();
	}

	private static EntityManager getEntityManager() {
		return EMF.get().createEntityManager();
	}
}
