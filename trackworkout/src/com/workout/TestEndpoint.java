package com.workout;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.config.Named;
import com.google.api.server.spi.response.CollectionResponse;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.oauth.OAuthRequestException;
import com.workout.pojo.Rep;
import com.workout.pojo.Set;
import com.workout.pojo.Workout;

@Api(name = "workout", version = "v1", namespace = @ApiNamespace(ownerDomain = "workout.com", ownerName = "workout.com", packagePath = "pojo"))
public class TestEndpoint {

	@ApiMethod(name = "listWorkout")
	public CollectionResponse<Workout> listWorkout(HttpServletRequest req) throws Exception {
		UserUtils.checkAuthorization(req);
		
		EntityManager mgr1 = getEntityManager();
		Query query = mgr1.createQuery("select from " + Workout.class.getName());
		List<Workout> workouts = (List<Workout>) query.getResultList();

		CollectionResponse<Workout> response = CollectionResponse.<Workout> builder().setItems(workouts).build();
		return response;
	}

	@ApiMethod(name = "insertWorkout")
	public void insertWorkout(Workout workout) throws OAuthRequestException {
		System.out.println("yo");
		System.out.println(workout);
		EntityManager mgr = getEntityManager();

		try {
			// save to database
			mgr.persist(workout);
		} finally {
			mgr.close();
		}
	}

	@ApiMethod(name = "removeRep", path = "workout/{workoutId}/set/{setId}/rep/{repId}")
	public void removeRep(@Named("workoutId") long workoutId, @Named("setId") long setId, @Named("repId") long repId) throws OAuthRequestException {
		Key workoutKey = KeyFactory.createKey(Workout.class.getSimpleName(), workoutId);
		Key setKey = KeyFactory.createKey(workoutKey, Set.class.getSimpleName(), setId);
		Key repKey = KeyFactory.createKey(setKey, Rep.class.getSimpleName(), repId);

		EntityManager em = getEntityManager();
		Rep rep = em.find(Rep.class, repKey);

		// if(rep == null)
		// throw new OAuthRequestException("Cant find rep");

		try {
			em.remove(rep);
		} finally {
			em.close();
		}
	}

	@ApiMethod(name = "removeSet", path = "workout/{workoutId}/set/{setId}")
	public void removeSet(@Named("workoutId") long workoutId, @Named("setId") long setId) throws OAuthRequestException {
		Key workoutKey = KeyFactory.createKey(Workout.class.getSimpleName(), workoutId);
		Key setKey = KeyFactory.createKey(workoutKey, Set.class.getSimpleName(), setId);
		
		EntityManager em = getEntityManager();
		Set set = em.find(Set.class, setKey);
        
		
		
		try {
			this.removeSetobject(set, em);
		} finally {
			em.close();
		}
	}
	private void removeSetobject(Set set, EntityManager em){
		
		List<Rep> reps = set.getReps();
		for (Rep rep : reps) {
			em.remove(rep);
		}

		// now remove set
		em.remove(set);
	}
	@ApiMethod(name = "removeWorkout", path = "workout/{workoutId}")
	public void removeWorkout(@Named("workoutId") long workoutId) throws OAuthRequestException {
		Key workoutKey = KeyFactory.createKey(Workout.class.getSimpleName(), workoutId);
		

		EntityManager em = getEntityManager();
		Workout workout = em.find(Workout.class, workoutKey);
		
		
		

		// if(rep == null)
		// throw new OAuthRequestException("Cant find rep");

		try {
			for (Set set:workout.getSets()){
				this.removeSetobject(set,em);
			}
			
			em.remove(workout);
		} finally {
			em.close();
		}
	}
	
	
	@ApiMethod(name = "insertSet", path = "set")
	public void insertSet(Set set) throws OAuthRequestException {
		System.out.println("yo");
		System.out.println(set);
		EntityManager mgr = getEntityManager();

		try {
			// save to database
			mgr.persist(set);
		} finally {
			mgr.close();
		}
		
	}
	
	@ApiMethod(name = "insertRep", path = "rep")
	public void insertRep(Rep rep) throws OAuthRequestException {
		System.out.println("yo");
		System.out.println(rep);
		EntityManager mgr = getEntityManager();

		try {
			// save to database
			mgr.persist(rep);
		} finally {
			mgr.close();
		}
		
	}
	
	@ApiMethod(name = "getWorkout")
	public Workout getWorkout(@Named("workoutId") long workoutId) throws OAuthRequestException {
Key workoutKey = KeyFactory.createKey(Workout.class.getSimpleName(), workoutId);
		

		EntityManager em = getEntityManager();
		Workout workout = em.find(Workout.class, workoutKey);
		return workout;
	}
	
	
	private static EntityManager getEntityManager() {
		// TODO Auto-generated method stub
		return EMF.get().createEntityManager();
	}
}
