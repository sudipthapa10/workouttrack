package com.workout;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.workout.pojo.Rep;

public class RepController extends HttpServlet {

	public void doPut(HttpServletRequest req, HttpServletResponse resp) throws IOException {

	}

	public void doDelete(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		System.out.println("Delete called");
		String repId = req.getPathInfo().replaceAll("/", "");

		EntityManager em = getEntityManager();

		Key key = KeyFactory.createKey(Rep.class.getSimpleName(), Long.parseLong(repId));
		Rep rep = em.find(Rep.class, key);

		System.out.println(repId);
		System.out.println(rep);

		Query query = em.createQuery("select from Rep where id = :id");
		query.setParameter("id", repId);
		
		List<Rep> reps = query.getResultList();
		System.out.println(reps);
		
		
		/**
		try {
			em.remove(rep);
		} finally {
			em.close();
		}
		*/
	}

	private static EntityManager getEntityManager() {
		return EMF.get().createEntityManager();
	}
}