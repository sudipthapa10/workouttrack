package com.workout;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiMethod.HttpMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.config.Named;
import com.google.api.server.spi.response.BadRequestException;
import com.google.api.server.spi.response.CollectionResponse;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.oauth.OAuthRequestException;
import com.workout.pojo.Token;
import com.workout.pojo.User;





@Api(name = "user", version = "v1", namespace = @ApiNamespace(ownerDomain = "workout.com", ownerName = "workout.com", packagePath = "pojo"))
public class UserEndpoint {

	@ApiMethod(name = "listUser")
	public CollectionResponse<User> listUser() throws OAuthRequestException {
		EntityManager mgr1 = getEntityManager();
		Query query = mgr1.createQuery("select from " + User.class.getName());
		List<User> users = (List<User>) query.getResultList();

		CollectionResponse<User> response = CollectionResponse.<User> builder().setItems(users).build();
		return response;
	}

	@ApiMethod(name = "insertUser")
	public Id insertUser(User user) throws BadRequestException {
		//System.out.println("yo");
		System.out.println(user);
		EntityManager mgr = getEntityManager();
		
		Query query = mgr.createQuery("select from " + User.class.getSimpleName() +" where email =:emailHolder");
		query.setParameter("emailHolder", user.getEmail());
		List<User>  users = query.getResultList();

		if (users.size() > 0)
			throw new BadRequestException("users already exists");


		if(user.getPassword()==null) {
			throw new BadRequestException("Please specify password");
		}

		if(user.getPasswordAgain()==null) {
			throw new BadRequestException("Please specify passwordAgain field");
		}
		
		if (!user.getPassword().equals(user.getPasswordAgain())) {
			throw new BadRequestException("Password dont match");
		}
		
		if(user.getEmail()==null || !user.getEmail().contains("@")) {
			throw new BadRequestException("Invalid email");
		}
		
		//update the hash
		try {
			user.setHash(user.getPassword().hashCode()+"");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			// save to database
			mgr.persist(user);
		} finally {
			mgr.close();
		}
		
		return new Id(user.getId().getId());
	}

	@ApiMethod(name = "removeUser",path = "user/{userId}")
	public void removeUser(@Named("userId") long userId) throws OAuthRequestException {
		Key userKey = KeyFactory.createKey(User.class.getSimpleName(), userId);
		//Key setKey = KeyFactory.createKey(workoutKey, Set.class.getSimpleName(), setId);
		//Key repKey = KeyFactory.createKey(setKey, Rep.class.getSimpleName(), repId);

		EntityManager em = getEntityManager();
		User users = em.find(User.class, userKey);

		 if(users == null)
		throw new OAuthRequestException("Cant find users");

		try {
			em.remove(users);
		} finally {
			em.close();
		}
	}

	
	
	
	private static EntityManager getEntityManager() {
		// TODO Auto-generated method stub
		return EMF.get().createEntityManager();
	}
	@ApiMethod(name = "getToken",path="user/token",httpMethod=HttpMethod.POST)
	public Token getToken(User passedUser) throws Exception {
		
		if(passedUser.getEmail() == null)
			throw new BadRequestException("Please specify email");
		
		if(passedUser.getPassword() == null)
			throw new BadRequestException("Please specify password");
		
		
		EntityManager em = getEntityManager();
		Query query = em.createQuery("select from " + User.class.getSimpleName() +" where email =:emailHolder");
		query.setParameter("emailHolder", passedUser.getEmail());
		
		User dbUser = (User) query.getSingleResult();
		System.out.println(dbUser.getName());
		//ensure the passed in user specifies the right password. Check with hash
		String passedHash = passedUser.getPassword().hashCode()+"";
		
		System.out.println("Passed hash " + passedHash);
		System.out.println("Db hash " + dbUser.getHash());
		
		//if so create a token. store it and then return
		if(passedHash.equals(dbUser.getHash())) {
			//create token. store and return
			String token = PasswordHash.createHash(Math.random() +"");
			dbUser.setToken(token);
			//save to db
			try {
				em.persist(dbUser);
			}finally {
				em.close();
			}
			
			Token tokenObj = new Token();
			tokenObj.setToken(token);
			return tokenObj;
		} else {
			throw new BadRequestException("Invalid password");
		}
		
	}
}
