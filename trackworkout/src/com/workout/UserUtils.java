package com.workout;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;

import com.workout.pojo.User;

public class UserUtils {

	public static User checkAuthorization(HttpServletRequest req) throws Exception {
			
		
		//get the user that has this token. 
		
			String token = req.getHeader("Authorization");
			if(token == null || token.length()<=0)
				throw new Exception("Please specify a token zzklj");
			
			EntityManager mgr1 = getEntityManager();
			
			Query query = mgr1.createQuery("select from " + User.class.getSimpleName()+ " where token=:token");
			query.setParameter("token", token);
			   List<User> users = (List<User>) query.getResultList();

			   System.out.println(token);
			   System.out.println(users);

			   // if the user is not null return user.
			   if (users.size() == 1) {
			   return users.get(0);
			   } else
			   throw new Exception("User not authorized");
			
		}
			
			
			
		//else throw Exception saying user not authorized
		
		
	
	private static EntityManager getEntityManager() {
		return EMF.get().createEntityManager();
	}
}
