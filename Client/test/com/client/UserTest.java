package com.client;


import javax.jws.soap.SOAPBinding;

import org.junit.Assert;
import org.junit.Test;

import com.client.http.FitnessClient;
import com.client.pojo.User;

public class UserTest {
	String hostname = "localhost";
	
			@Test(expected = Exception.class)
			public void testInvalidEmail() throws Exception {
				FitnessClient fc = new FitnessClient(hostname, 8888);
				User user = new User("name", "abcinvaidEmail", "ppp");
				user.setPasswordAgain("ppp");
				fc.getUserClient().createUser(user);
			}

	@Test(expected=Exception.class)
	public void testInvalidEmailContent() {
		FitnessClient fc = new FitnessClient(hostname, 8888);
		User user = new User("name", "abcinvaidEmail", "ppp");
		user.setPasswordAgain("ppp");
		/*try {
			fc.getUserClient().createUser(user);
		} catch (Exception e) {
			Assert.assertEquals("Error creating user : Invalid email", e.getMessage());
		}*/
	}
	

	@Test
	public void testUserCreation() throws Exception {
		FitnessClient fc = new FitnessClient(hostname, 8888);
		User user = new User("name", "jabc@aa.com", "ppp");
		user.setPasswordAgain("ppp");
		
		long userId = fc.getUserClient().createUser(user);
		System.out.println(userId);
		
		fc.getUserClient().deleteUser(userId);
		//TODO: Extra validation. do a get on /user and make sure the user we just created is available
	}
	
	@Test(expected=Exception.class)
	public void testCantCreateTwoUserWithSameEmail() throws Exception {
		FitnessClient fc = new FitnessClient(hostname, 8888);
		String random = Double.toString(Math.random());
		User user = new User("name", "aaa" + random + "@aa.com", "ppp");
		user.setPasswordAgain("ppp");
		//create first user
		fc.getUserClient().createUser(user);
		
		Thread.sleep(2000);
		//create second user with the same email. TODO: this dosnt throw exception. should throw bercase we are creatign two user with the
		//same email
		fc.getUserClient().createUser(user);
		
	}
	@Test
	public void mismatchPassword()throws Exception{
		FitnessClient fc= new FitnessClient(hostname,8888);
		User user= new User();
		user.setName("himal");
		user.setEmail("abc@gmail.com");
		user.setPassword("abcdef");
		user.setPasswordAgain("abcdef");
		fc.getUserClient().createUser(user);
	}
	@Test(expected =Exception.class)
	public void emailcheck()throws Exception{
		FitnessClient fc= new FitnessClient(hostname,8888);
		String random = Double.toString(Math.random());
		User user = new User("name", "aaa" + random + "aa.com", "ppp");
		user.setPasswordAgain("ppp");
		//create first user
		fc.getUserClient().createUser(user);
		
		Thread.sleep(2000);
		//create second user with the same email. TODO: this dosnt throw exception. should throw bercase we are creatign two user with the
		//same email
		fc.getUserClient().createUser(user);
	}
	
	
	@Test
	public void createUserWithoutPassword()throws Exception{
		FitnessClient fc= new FitnessClient(hostname, 8888);
	    User user= new User();
	    user.setName("abcdef");
	    user.setEmail("abcedf@hotmail.com");
	    fc.getUserClient().createUser(user);
	}
}








