package com.client.pojo;

public class Rep {
	private int repcount;
	private int weight;

	public Rep(int repcount, int weight) {
		this.repcount = repcount;
		this.weight = weight;
	}

	public int getRepcount() {
		return repcount;
	}

	public void setRepcount(int repcount) {
		this.repcount = repcount;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	

}
