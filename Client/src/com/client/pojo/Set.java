package com.client.pojo;

import java.util.List;

public class Set {
	List<Rep> reps;

	public Set(List<Rep> reps) {
		this.reps = reps;
	}

	public List<Rep> getReps() {
		return reps;
	}

	public void setReps(List<Rep> reps) {
		this.reps = reps;
	}

}
