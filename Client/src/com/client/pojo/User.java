package com.client.pojo;

import java.beans.Transient;

public class User {

	private String email;

	
	private String name;

	private String password;

	
	private String passwordAgain;

	private String token;
	public User( String name, String email, String password){
		this.name=name;
		this.email=email;
		this.password=password;
	}
	

	public User() {
		// TODO Auto-generated constructor stub
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordAgain() {
		return passwordAgain;
	}

	public void setPasswordAgain(String passwordAgain) {
		this.passwordAgain = passwordAgain;
	}

}
