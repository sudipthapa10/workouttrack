package com.client.pojo;


import java.util.List;



public class Workout {
	
	List<Set> sets;
	
	private String name;

	private boolean isSuperset;
	
	private boolean isDropset;

	public List<Set> getSets() {
		return sets;
	}

	public void setSets(List<Set> sets) {
		this.sets = sets;
	}

	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isSuperset() {
		return isSuperset;
	}

	public void setSuperset(boolean isSuperset) {
		this.isSuperset = isSuperset;
	}

	public boolean isDropset() {
		return isDropset;
	}

	public void setDropset(boolean isDropset) {
		this.isDropset = isDropset;
	}

	
}
