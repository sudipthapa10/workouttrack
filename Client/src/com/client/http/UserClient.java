package com.client.http;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;

import com.client.pojo.Id;
import com.client.pojo.User;
import com.google.gson.Gson;

public class UserClient extends BaseClient {

	public UserClient(String endpoint, int port) {
		super(endpoint, port);
		// TODO Auto-generated constructor stub
	}

	public long createUser(User user) throws Exception{
		Response response = this.httpTransport.post("/_ah/api/user/v1/id", toJson(user));
		System.out.println(response.getStatusCode());
		if(response.getStatusCode() != 204 && response.getStatusCode() != 200) {
			throw new Exception("Error creating user : " + getMessage(response));
		} else if(response.getStatusCode() == 200){
			String str = response.getResponse();
			Gson gson = new Gson();
			Id id = gson.fromJson(str, Id.class);
			return id.getId();
		} else {
			return -1;
		}
	}
	
	private String getMessage(Response response) {
		String str = response.getResponse();
		str = str.substring(str.indexOf("message\" : ") + 12, str.length());
		str =str.substring(0, str.indexOf("\","));
		return str;
	}

	private String toJson(User user) {
		Gson gson = new Gson();
		return gson.toJson(user);
	}

	public void deleteUser(long userId) throws ClientProtocolException, IOException {
		this.httpTransport.delete("/_ah/api/user/v1/user/" + userId);
		
	}
}
