package com.client.http;

import java.util.ArrayList;
import java.util.List;

import com.client.pojo.Rep;
import com.client.pojo.Set;
import com.client.pojo.Workout;

public class WorkoutClient extends BaseClient  {

	public WorkoutClient(String endpoint, int port) {
		super(endpoint, port);
		// TODO Auto-generated constructor stub
	}
	 public static void main(String[] args) {
		Workout workout= new Workout();
		workout.setName(" Benchpress");
		workout.setSuperset(true);
		List<Rep> reps = new ArrayList<Rep>();
		reps.add(new Rep(3, 20));

		reps.add(new Rep(5, 15));

		List<Set> sets = new ArrayList<Set>();
		sets.add(new Set(reps));
		
		
		workout.setSets(sets);
		
		
	}

		
		
	}

