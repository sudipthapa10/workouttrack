package com.client.http;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.client.ClientProtocolException;

import com.client.pojo.Rep;
import com.client.pojo.Set;
import com.client.pojo.Token;
import com.client.pojo.User;
import com.client.pojo.Workout;
import com.google.gson.Gson;

public class OldWorkoutClient {

	public void post() throws ClientProtocolException, IOException {
		HttpTransportClient httpClient = new HttpTransportClient("localhost",
				8888);

		List<Rep> reps = new ArrayList<Rep>();
		reps.add(new Rep(3, 20));

		reps.add(new Rep(5, 15));

		List<Set> sets = new ArrayList<Set>();
		sets.add(new Set(reps));
		// sets.add(new Set(rep1));
		// sets.add(new Set(reps));
		Workout workout = new Workout();

		workout.setName("shoulder");

		workout.setSets(sets);

		System.out.println(toJson(workout));

		Response response = httpClient.post("/_ah/api/workout/v1/void",
				toJson(workout));
		System.out.println(response.getStatusCode());
		System.out.println(response.getResponse());
	}

	public void get() throws ClientProtocolException, IOException {
		String token = getToken();
		
		HttpTransportClient httpClient = new HttpTransportClient("localhost",
				8888);

		Response response1 = httpClient.get("/_ah/api/workout/v1/workout", token);
		System.out.println(response1);
		System.out.println(response1.getResponse());

	}

	public String getToken() throws ClientProtocolException, IOException {
		HttpTransportClient httpClient = new HttpTransportClient("localhost", 8888);
		User user = new User();
		user.setEmail("abc4@hotmail.com");
		user.setPassword("1234");
		
		Response response = httpClient.post("/_ah/api/user/v1/user/token", toJson(user));
		String responseStr = response.getResponse();
		Token token = fromJson(responseStr, Token.class);
				
		return token.getToken();
	}

	private Token fromJson(String str, Class<Token> clazz) {
		Gson gson = new Gson();
		return gson.fromJson(str, clazz);
	}

	public static void main(String[] args) throws ClientProtocolException,
			IOException {
		// new WorkoutClient().post();
		new OldWorkoutClient().get();

	}

	private String toJson(Object obj) {
		Gson gson = new Gson();
		return gson.toJson(obj);
	}
}