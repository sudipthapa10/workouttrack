package com.client.http;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;

import com.client.pojo.User;

public class FitnessClient {

	private UserClient userClient;
	private WorkoutClient workoutClient;

	public FitnessClient(String endpoint, int port) {
		this.userClient = new UserClient(endpoint, port);
		this.workoutClient = new WorkoutClient(endpoint, port);
	}

	public static void main(String[] args) throws Exception {

		FitnessClient fc = new FitnessClient("localhost", 8888);
		User user = new User("helll01", " hello2 @gmail.com", "hello 34");
		user.setPasswordAgain("hello 34");
		fc.getUserClient().createUser(user);
		

	}

	public UserClient getUserClient() {
		return this.userClient;
		
	}
}