package com.client.http;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;

import com.client.pojo.User;
import com.google.gson.Gson;

public class OldUserClient {
	public void get() throws ClientProtocolException, IOException {
		HttpTransportClient httpClient = new HttpTransportClient("localhost",8888);
		Response response = httpClient.get("/_ah/api/user/v1/user");
		System.out.println(response.getStatusCode());
		System.out.println(response.getResponse());
			
	}
	
	public void post() throws ClientProtocolException, IOException {
		HttpTransportClient httpClient = new HttpTransportClient("localhost",8888);
		String body = "{\"email\": \"abc4@hotmail.com\",\"name\": \"abc\",\"password\": \"1234\",\"passwordAgain\": \"1234\"}";
		
		Response response = httpClient.post("/_ah/api/user/v1/void", body);
		//System.out.println(response.getStatusCode());
		//System.out.println(response.getResponse());
	}
	
	

	public void post1() throws ClientProtocolException, IOException {
		HttpTransportClient httpClient = new HttpTransportClient("localhost",8888);
	
		User user = new User("name", "a@s.com", "abcdef");

		/*
		User user = new User();
		user.setName("user1");
		user.setEmail("a@b.com");
		user.setPassword("abc");
		user.setPasswordAgain("abc");
		*/
		
		Response response = httpClient.post("/_ah/api/user/v1/void", toJson(user));
		//System.out.println(response.getStatusCode());
		//System.out.println(response.getResponse());
	}
	
	
	private String toJson(User user) {
		Gson gson = new Gson();
		return gson.toJson(user);
	}

	public static void main(String[] args) throws ClientProtocolException, IOException {
		new OldUserClient().get();
		
		new OldUserClient().post1();		
	}
}
